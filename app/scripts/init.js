'use strict';
var $ = window.$;

$(document).ready(function () {

  $.fancybox.defaults.touch = false;
  $.fancybox.defaults.toolbar = false;
  $.fancybox.defaults.infobar = false;
  $.fancybox.defaults.arrows = false;
  $.fancybox.defaults.autoFocus = false;
  $.fancybox.defaults.closeExisting = true;

  $('.callback-form form').on('submit', function (e){
  	e.preventDefault();
 	 $.fancybox.open( $('#callback-success'));
      sendAjaxForm($(this));
  })

    function sendAjaxForm(form) {
        $.ajax({
            url: '/send_form.php',
            type: "POST",
            dataType: "html",
            data: form.serialize(),
            success: function success(response) {
                form.find('.submit-response').text('');
            },
            error: function error(response) {
                $.fancybox.open($('callback-fail'));
            }
        });
    }


    // Слайдеры
    $('[demo-slide]').each(function(){
        let $block = $(this);
        let $init = $block.find('[class*="__list"]');
        let $dots = $block.find('[class*="-dots"]');
        let $prev = $block.find('[class*="_prev"]');
        let $next = $block.find('[class*="_next"]');

        $init.slick({
            arrows: true,
            dots: true,
            variableWidth: true,
            centerMode: true,
            appendDots: $dots,
            prevArrow: $prev,
            nextArrow: $next,
        })
    })

    window.onload = function(){
        sliderAlign('[demo-slide]');
    }

    $(window).on('resize', function(){
        sliderAlign('[demo-slide]');
    });

    function sliderAlign(slider) {
        $(slider).each(function(index, element){
            var slickTrack = $(element).find('.slick-track');
            slickTrack.css('height', 'auto');
            var sliderHeight = slickTrack.height();
            slickTrack.css('height', sliderHeight + 'px');
        });
    }
});